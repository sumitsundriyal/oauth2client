/**
 * 
 */
package com.sumsoft.controller;

import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class is used for access client token.
 * @author sumit
 *
 */
@RestController
@EnableOAuth2Client
public class OAuth2ClientController {
	@RequestMapping(value="/getToken", method=RequestMethod.GET,headers="Accept=application/json")
	@ResponseBody
	public String getOAuthToken(){
		BaseOAuth2ProtectedResourceDetails resource = new ClientCredentialsResourceDetails();
		resource.setAccessTokenUri("http://localhost:8081/oauth2server/oauth/token");
		resource.setClientId("sumitappclient");
		resource.setClientSecret("secret");
		resource.setGrantType("client_credentials");
		
		OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(resource);
		OAuth2AccessToken oAuth2AccessToken = oAuth2RestTemplate.getAccessToken();
		return "Access Token is "+ oAuth2AccessToken.getValue();
	}
	
	

}
